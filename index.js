let request = require('request');
var http = require('http');

let apiKey = 'c0c25aac328c6375c30f10d833e0d88d';
let city = 'portland';
let url = `http://api.openweathermap.org/data/2.5/weather?q=${city}&APPID=${apiKey}`;
let message = '';
request(url, function (err, response, body) {
    if(err){
      console.log('error:', error);
    } else {
      let weather = JSON.parse(body)
      message = `It's ${weather.main.temp} degrees in ${weather.name}!`;
      console.log(message);
    }
  });
  console.log('server running...http://localhost:8080/');
//create a server object:
http.createServer(function (req, res) {    
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(message); //write a response to the client
    res.end(); //end the response
  }).listen(process.env.PORT || 8080); //the server object listens on port 8080

